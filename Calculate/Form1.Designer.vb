﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.reset = New System.Windows.Forms.Button()
        Me.calculator = New System.Windows.Forms.Button()
        Me.result = New System.Windows.Forms.TextBox()
        Me.labelFirstNumber = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.firstNumber = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.secondNumber = New System.Windows.Forms.TextBox()
        Me.operators = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'reset
        '
        Me.reset.Location = New System.Drawing.Point(25, 242)
        Me.reset.Name = "reset"
        Me.reset.Size = New System.Drawing.Size(75, 23)
        Me.reset.TabIndex = 4
        Me.reset.Text = "&Reset"
        Me.reset.UseVisualStyleBackColor = True
        '
        'calculator
        '
        Me.calculator.Location = New System.Drawing.Point(184, 240)
        Me.calculator.Name = "calculator"
        Me.calculator.Size = New System.Drawing.Size(93, 25)
        Me.calculator.TabIndex = 3
        Me.calculator.Text = "&Calculate"
        Me.calculator.UseVisualStyleBackColor = True
        '
        'result
        '
        Me.result.Location = New System.Drawing.Point(25, 12)
        Me.result.Multiline = True
        Me.result.Name = "result"
        Me.result.ReadOnly = True
        Me.result.Size = New System.Drawing.Size(260, 33)
        Me.result.TabIndex = 6
        '
        'labelFirstNumber
        '
        Me.labelFirstNumber.AutoSize = True
        Me.labelFirstNumber.Location = New System.Drawing.Point(22, 87)
        Me.labelFirstNumber.Name = "labelFirstNumber"
        Me.labelFirstNumber.Size = New System.Drawing.Size(89, 17)
        Me.labelFirstNumber.TabIndex = 3
        Me.labelFirstNumber.Text = "First Number"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 135)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 17)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Operator"
        '
        'firstNumber
        '
        Me.firstNumber.Location = New System.Drawing.Point(150, 80)
        Me.firstNumber.Multiline = True
        Me.firstNumber.Name = "firstNumber"
        Me.firstNumber.Size = New System.Drawing.Size(131, 27)
        Me.firstNumber.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 182)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Second Number"
        '
        'secondNumber
        '
        Me.secondNumber.Location = New System.Drawing.Point(150, 179)
        Me.secondNumber.Multiline = True
        Me.secondNumber.Name = "secondNumber"
        Me.secondNumber.Size = New System.Drawing.Size(131, 28)
        Me.secondNumber.TabIndex = 2
        '
        'operators
        '
        Me.operators.FormattingEnabled = True
        Me.operators.Items.AddRange(New Object() {"+", "-", "*", "/"})
        Me.operators.Location = New System.Drawing.Point(150, 132)
        Me.operators.Name = "operators"
        Me.operators.Size = New System.Drawing.Size(131, 24)
        Me.operators.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(316, 295)
        Me.Controls.Add(Me.operators)
        Me.Controls.Add(Me.secondNumber)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.firstNumber)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.labelFirstNumber)
        Me.Controls.Add(Me.result)
        Me.Controls.Add(Me.calculator)
        Me.Controls.Add(Me.reset)
        Me.Name = "Form1"
        Me.RightToLeftLayout = True
        Me.Text = "Calculate"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents reset As System.Windows.Forms.Button
    Friend WithEvents calculator As System.Windows.Forms.Button
    Friend WithEvents result As System.Windows.Forms.TextBox
    Friend WithEvents labelFirstNumber As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents firstNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents secondNumber As System.Windows.Forms.TextBox
    Friend WithEvents operators As System.Windows.Forms.ComboBox

End Class
